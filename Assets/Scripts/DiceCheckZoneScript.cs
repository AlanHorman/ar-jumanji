﻿using UnityEngine;

public class DiceCheckZoneScript : MonoBehaviour {

	Vector3 diceVelocity;

	void FixedUpdate () {
		diceVelocity = DiceScript.diceVelocity;
	}

	void OnTriggerStay(Collider col)
	{
		if (diceVelocity.x == 0f && diceVelocity.y == 0f && diceVelocity.z == 0f)
		{
			switch (col.gameObject.name) {
			case "Side1":
				RandomCube.diceNumber = 6;
				break;
			case "Side2":
				RandomCube.diceNumber = 5;
				break;
			case "Side3":
				RandomCube.diceNumber = 4;
				break;
			case "Side4":
				RandomCube.diceNumber = 3;
				break;
			case "Side5":
				RandomCube.diceNumber = 2;
				break;
			case "Side6":
				RandomCube.diceNumber = 1;
				break;
			}
		}
	}
}