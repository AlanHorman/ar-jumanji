﻿using UnityEngine;

public class DestroySpawnedEnemies : MonoBehaviour
{
    public Camera arCamera;
    private Spider spider;
    
    void Update()
    {
        if ((Input.GetTouch(0).phase == TouchPhase.Stationary) || 
            (Input.GetTouch(0).phase == TouchPhase.Moved && Input.GetTouch(0).deltaPosition.magnitude < 1.2f))
        {
            Ray ray = arCamera.ScreenPointToRay(Input.GetTouch(0).position);
            RaycastHit hit;

            if (Physics.Raycast(ray, out hit))
            {
                switch (hit.transform.tag)
                {
                    case "Spider":

                        Spider _spider = hit.transform.gameObject.GetComponent<Spider>();
                        _spider.Die();
                        break;
                    case "Skeleton":

                        Skeleton _skeleton = hit.transform.gameObject.GetComponent<Skeleton>();
                        _skeleton.Die();
                        break;
                    default:
                        break;
                }
            }
        }
    }
}