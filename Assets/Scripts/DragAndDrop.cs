﻿using System.Collections;
using System.Collections.Generic;
using Unity.Collections;
using UnityEngine;
using UnityEngine.XR.ARFoundation;
using UnityEngine.XR.ARSubsystems;

public class DragAndDrop : MonoBehaviour
{
    public Camera arCamera;
    public Material inactive;
    public Material active;

    public GameObject X;
    public GameObject Y;
    public GameObject Z;
    public GameObject Moving;
    public GameObject Rotation;

    private bool x = false;
    private bool y = false;
    private bool z = false;
    private bool rotation = false;
    private bool moving = false;

    private float rx = 0f;
    private float ry = 0f;
    private float rz = 0f;
    
    private GameObject place;
    private bool draggable = false;
    private Vector2 touchPosition;
    private ARRaycastManager arRaycastManager;
    private static List<ARRaycastHit> hits = new List<ARRaycastHit>();

    void Awake()
    {
        arRaycastManager = GetComponent<ARRaycastManager>();
    }
    
    void Update()
    {
        if (draggable)
        {
            Move();
        }

        if (Input.touchCount > 0)
        {
            Touch touch = Input.GetTouch(0);

            touchPosition = touch.position;

            if ((Input.GetTouch(0).phase == TouchPhase.Began))
            {
                Ray ray = arCamera.ScreenPointToRay(Input.GetTouch(0).position);
                RaycastHit hitObject;

                if (Physics.Raycast(ray, out hitObject, 100f))
                {
                    if (hitObject.transform.name.Contains("X"))
                    {
                        if (!x)
                        {
                            x = true;
                            X.GetComponent<Renderer>().material = active;
                        }
                        else
                        {
                            x = false;
                            X.GetComponent<Renderer>().material = inactive;
                        }

                    }

                    if (hitObject.transform.name.Contains("Y"))
                    {
                        if (!y)
                        {
                            y = true;
                            Y.GetComponent<Renderer>().material = active;
                        }
                        else
                        {
                            y = false;
                            Y.GetComponent<Renderer>().material = inactive;
                        }
                    }
                    
                    if (hitObject.transform.name.Contains("Z"))
                    {
                        if (!z)
                        {
                            z = true;
                            Z.GetComponent<Renderer>().material = active; 
                        }
                        else
                        {
                            z = false;
                            Z.GetComponent<Renderer>().material = inactive;
                        }
                    }
                    
                    if (hitObject.transform.name.Contains("Moving"))
                    {
                        if (!moving)
                        {
                            moving = true;
                            Moving.GetComponent<Renderer>().material = active; 
                        }
                        else
                        {
                            moving = false;
                            Moving.GetComponent<Renderer>().material = inactive;
                        }
                    }
                    
                    if (hitObject.transform.name.Contains("Rotation"))
                    {
                        if (!rotation)
                        {
                            rotation = true;
                            Rotation.GetComponent<Renderer>().material = active; 
                        }
                        else
                        {
                            rotation = false;
                            Rotation.GetComponent<Renderer>().material = inactive;
                        }
                    }
                    

                    if (hitObject.transform.name.Contains("Cube"))
                    {
                        place = hitObject.transform.gameObject;
                        draggable = true;
                        place.GetComponent<Outline>().enabled = true;
                    }
                }
            }
            
            if ((Input.GetTouch(0).phase == TouchPhase.Ended))
            {
                draggable = false;
                place.GetComponent<Outline>().enabled = false;

                place = null;

                rx = 0f;
                ry = 0f;
                rz = 0f;
            }
        }
        
        
         
    }
    
    void Move()
    {
        Ray ray = arCamera.ScreenPointToRay(Input.GetTouch(0).position);
        RaycastHit hitObject;

        if (Physics.Raycast(ray, out hitObject, 100f))
        {
            if (hitObject.transform.name.Contains("Cube") && moving)
            {
                place.transform.position = new Vector3(x ? hitObject.point.x : place.transform.position.x, 
                                                          y ? hitObject.point.y : place.transform.position.y, 
                                                            z ? hitObject.point.z : place.transform.position.z);
            }
            
            if (hitObject.transform.name.Contains("Cube") && rotation)
            {
                if (x)
                {
                    place.transform.Rotate(x ? rx += 0.001f : place.transform.rotation.x , 0, 0);
                }

                if (y)
                {
                    place.transform.Rotate(0, y ? ry += 0.001f : place.transform.rotation.y, 0);
                }

                if (z)
                {
                    place.transform.Rotate(0, 0, z ? rz += 0.001f : place.transform.rotation.z);
                }
                //place.transform.Rotate(x ? rx += 0.001f : place.transform.rotation.x, 
                  //  y ? ry += 0.001f : place.transform.rotation.y, 
                    //z ? rz += 0.001f : place.transform.rotation.z);
            }
        }
    }
}
