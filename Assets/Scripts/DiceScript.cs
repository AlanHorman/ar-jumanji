﻿using UnityEngine;

public class DiceScript : MonoBehaviour {

	static Rigidbody rb;
	public static Vector3 diceVelocity;
	private static float RANDOM_X;
	private static float RANDOM_Y;
	private static float RANDOM_Z;
	
	void Start () {
		rb = GetComponent<Rigidbody> ();
		RANDOM_X = Random.Range(0.0f, 360.0f);
		RANDOM_Y = Random.Range(0.0f, 360.0f);
		RANDOM_Z = Random.Range(0.0f, 360.0f);
		transform.rotation = Quaternion.Euler(RANDOM_X, RANDOM_Y, RANDOM_Z);
	}
	
	void Update () {
		diceVelocity = rb.velocity;
		
		if (Input.GetKeyDown(KeyCode.Space))
		{
			float dirX = Random.Range(0, 500);
			float dirY = Random.Range(0, 500);
			float dirZ = Random.Range(0, 500);
			transform.position = new Vector3(0, 10, 0);
			transform.rotation = Quaternion.identity;
			rb.AddForce(transform.up * 500);
			rb.AddTorque(dirX, dirY, dirZ);
		}
	}
}