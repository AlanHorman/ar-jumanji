﻿using UnityEngine;
using UnityEngine.UI;

public class InstructionTextViewButtons : MonoBehaviour
{
    public GameObject startMenu;
    public GameObject instructionTextView;
    public Button back;
    
    void Start()
    {
        back.onClick.AddListener(Back);
    }

    void Back()
    {
        instructionTextView.SetActive(false);
        startMenu.SetActive(true);
    }
}