﻿using UnityEngine;

public class SphereEventScript : MonoBehaviour
{
    private TextMesh testMesh;
    public Animator anim;
    public SpawningEnemies spawningEnemies;
    public GameObject spawningEvent;

    void Start()
    {
        anim = GetComponent<Animator>();       
        spawningEvent = GameObject.Find("SpawningEvent");
        spawningEnemies = spawningEvent.GetComponent<SpawningEnemies>();
    }

    public void AnimateMessage()
    {
        Debug.Log("Calling AnimateMessage()");
        RandomMessage();
        anim.Play("textanimation", -1, 0f);
    }

    private void RandomMessage()
    {
        testMesh = GetComponent<TextMesh>();
        int randomNum = Random.Range(1, 13);

        if (randomNum == 1 || randomNum == 6 || randomNum == 9 || randomNum == 12 )
        {
            this.testMesh.text = "IT' TIME\nTO BE BRAVE.\n SOMETHING\nWAS AWAKENED\nFROM ITS\nGRAVE.";      
            spawningEnemies.SpawnASkeleton();
        }
        else if (randomNum == 2 || randomNum == 5|| randomNum == 8 || randomNum == 11)
        {
            this.testMesh.text = "NEED A HAND?\nWHY YOU JUST WAIT.\nWE'LL\nHELP YOU OUT.\nWE EACH\nHAVE EIGHT";     
            spawningEnemies.SpawnSpiders();
        }
        else if (randomNum == 3 || randomNum == 4 || randomNum == 7 || randomNum == 10)
        {
            this.testMesh.text = "BETTER WATCH\nYOUR BACK\nWHEN THE\nZULU ATTACK.";
            spawningEnemies.SpawnFallingSpears();
        }
    }
    
    public void Jumanji()
    {
        this.testMesh.text = "Jumanji";
        anim.Play("textanimation", -1, 0f);
    }
}