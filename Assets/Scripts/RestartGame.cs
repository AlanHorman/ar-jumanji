﻿using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class RestartGame : MonoBehaviour
{
    public GameObject startMenu;
    public Button startMenuButton;
    public GameObject gameEndScreen;

    public void Start()
    {
        startMenuButton.onClick.AddListener(ReturnToStart);
    }

    public void ReturnToStart()
    {
        gameEndScreen.SetActive(false);
        startMenu.SetActive(true);
        SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex);
    }
}