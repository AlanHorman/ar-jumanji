﻿using UnityEngine;
using UnityEngine.UI;

public class PlacingBoard : MonoBehaviour
{
    public Button back;
    public Button next;
    public GameObject placingBoardMenu;
    public GameObject planeGenerationMenu;
    public GameObject chooseFigureMenu;
    
    public void Start()
    {
        next.onClick.AddListener(Next);
        back.onClick.AddListener(Back);
    }

    public void Next()
    {
        placingBoardMenu.SetActive(false);
        chooseFigureMenu.SetActive(true);
        SpawnObjectOnPlane.isPlaced = true;
    }

    public void Back()
    {
        planeGenerationMenu.SetActive(true);
        placingBoardMenu.SetActive(false);
    }
}