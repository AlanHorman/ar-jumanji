﻿using UnityEngine;
using UnityEngine.UI;
using UnityEngine.XR.ARSubsystems;

public class PlaneGenerationMenuButtons : MonoBehaviour
{
    public Button back;
    public Button next;
    public GameObject startMenu;
    public GameObject planeGenerationMenu;
    public GameObject placingBoardMenu;
    public GameObject chooseFigureMenu;
    public static bool isPlaced;
    
    public void Start()
    {
        isPlaced = false;
        next.interactable = false;

        next.onClick.AddListener(Next);
        back.onClick.AddListener(Back);
    }

    private void Update()
    {
        if (isPlaced)
        {
            next.interactable = true;
        }
    }

    public void Next()
    {
        SpawnObjectOnPlane.isPlaced = true;
        SpawnObjectOnPlane.SetAllPlanesActive(false);
        planeGenerationMenu.SetActive(false);
        chooseFigureMenu.SetActive(true);
    }

    public void Back()
    {
        isPlaced = false;
        SpawnObjectOnPlane.isPlaced = false;
        SpawnObjectOnPlane.SetAllPlanesActive(false);
        SpawnObjectOnPlane.planeManager.detectionMode = PlaneDetectionMode.None;
        next.interactable = false;
        Destroy(SpawnObjectOnPlane.spawnedObject);
        startMenu.SetActive(true);
        planeGenerationMenu.SetActive(false);
    }
}