﻿using UnityEngine;

public class Skeleton : MonoBehaviour
{
    private CharacterController controller;
    private Animator anim;
    private GameObject player;
    public float _distance;
    public GameObject healthbar;
    public PlayerHealth playerhealth;
    private bool isAlive;
    
    void Start()
    {
        anim = GetComponent<Animator>();
        player = GameObject.FindGameObjectWithTag("PlayerFigure");
        healthbar = GameObject.Find("HealthBar");
        playerhealth = healthbar.GetComponent<PlayerHealth>();
        isAlive = true;
    }

    void Update()
    {
        if (isAlive)
        {
            _distance = Vector3.Distance(player.transform.position, transform.position);
            Quaternion targetRotation = Quaternion.LookRotation(player.transform.position - transform.position);
            transform.rotation = Quaternion.Slerp(transform.rotation, targetRotation, 1 * Time.deltaTime);

            if (!(_distance < 0.02))
            {
                anim.SetInteger("condition", 1);
                transform.position += transform.forward * 0.01f * Time.deltaTime;
            }
            else
            {
                anim.SetInteger("condition", 2);
            }
        }
    }

    public void AttackPlayer()
    {
        playerhealth.TakeDamage(1);    
    }
    
    public void Die()
    {
        isAlive = false;
        anim.SetInteger("condition", 3);
        Destroy(this.gameObject, 3f);
    }
}