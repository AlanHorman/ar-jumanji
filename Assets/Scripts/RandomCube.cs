﻿using System.Collections;
using UnityEngine;
using UnityEngine.UI;

public class RandomCube : MonoBehaviour
{
    public Button dice;
    private static WaypointController controller;
    private GameObject player;
    public Sprite one;
    public Sprite two;
    public Sprite three;
    public Sprite four;
    public Sprite five;
    public Sprite six;
    private static Vector3 dicePlace;
    public GameObject dice3D;
    private static GameObject spawnedObject;
    public static int diceNumber;
    public static bool rolled;
    
    public void Setup(GameObject figure)
    {
        Debug.Log("1");
        player = figure;
        Debug.Log("2");
        controller = player.GetComponent<WaypointController>();
        Debug.Log("3");
        dicePlace = SpawnObjectOnPlane.spawnedObject.transform.position;
        Debug.Log("4");
        dicePlace.x += 0.2f;
        dicePlace.y = 0.1f;
        dice.onClick.AddListener(RollDice);
    }
    
    private void Update()
    {
        switch (diceNumber)
        {
            case 1:
                this.dice.GetComponent<Image>().sprite = one;
                break;
            case 2:
                this.dice.GetComponent<Image>().sprite = two;
                break;
            case 3:
                this.dice.GetComponent<Image>().sprite = three;
                break;
            case 4:
                this.dice.GetComponent<Image>().sprite = four;
                break;
            case 5:
                this.dice.GetComponent<Image>().sprite = five;
                break;
            case 6:
                this.dice.GetComponent<Image>().sprite = six;
                break;
            default:
                break;
        }

        if (rolled)
        {
            rolled = false;
            StartCoroutine(GetNumber());
        }
    }

    public void SetTargetWayPointJumanji(int n)
    {
        if (n != 0)
        {
            if (controller.GetLastWaypointIndex() + n < controller.waypoints.Count)
            {
                controller.SetTargetWaypoint(controller.waypoints[controller.GetTargetWaypointIndex()]);
                controller.IncreaseLastWayPointIndex(n);
            }
        }

        StartCoroutine(DiceInteractable());
    }
    
    public void RollDice()
    {
        dice.interactable = false;
        spawnedObject = Instantiate(dice3D, dicePlace, Quaternion.identity);
        rolled = true;
    }

    IEnumerator GetNumber()
    {

        yield return new WaitForSeconds(2f);
        Destroy(spawnedObject);
        Debug.LogError(diceNumber);
        SetTargetWayPointJumanji(diceNumber);
    }

    IEnumerator DiceInteractable()
    {
        yield return new WaitForSeconds(2);
        dice.interactable = true;
    }
}