﻿using UnityEngine;

public class HitSpear : MonoBehaviour
{
    public GameObject healthbar;
    public PlayerHealth playerhealth;
    
    void Start()
    {
        healthbar = GameObject.Find("HealthBar");
        playerhealth = healthbar.GetComponent<PlayerHealth>();
    }
    
    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.CompareTag("Spear"))
        {
            playerhealth.TakeDamage(1);   
        }
    }
}