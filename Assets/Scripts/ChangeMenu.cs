﻿using UnityEngine;

public class ChangeMenu : MonoBehaviour
{
    public GameObject startMenu;
    public static bool startMenuClosed = false;
    
    void Start()
    {
        startMenu = GameObject.Find("Start Menu");
    }
    
    void Update()
    {
        if (startMenuClosed)
        {
            startMenu.SetActive(true);
        }
        else
        {
            startMenu.SetActive(false);
        }
    }
}