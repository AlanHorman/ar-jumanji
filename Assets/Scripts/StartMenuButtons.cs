﻿using UnityEngine;
using UnityEngine.UI;
using UnityEngine.XR.ARFoundation;
using UnityEngine.XR.ARSubsystems;

public class StartMenuButtons : MonoBehaviour
{
    public Button exit;
    public Button instruction;
    public Button start;
    public GameObject startMenu;
    public GameObject instructionTextView;
    public GameObject planeGenerationMenu;

    public void Start()
    {
        exit.onClick.AddListener(CloseGame);
        instruction.onClick.AddListener(ShowInstruction);
        start.onClick.AddListener(StartGame);
    }

    public void CloseGame()
    {
        Application.Quit();
    }
    
    public void StartGame()
    {
        startMenu.SetActive(false);
        planeGenerationMenu.SetActive(true);
        SpawnObjectOnPlane.SetAllPlanesActive(true);
        SpawnObjectOnPlane.planeManager.detectionMode = PlaneDetectionMode.Horizontal;
    }

    private void Update()
    {
        SpawnObjectOnPlane.planeManager.detectionMode = PlaneDetectionMode.None;
        SpawnObjectOnPlane.SetAllPlanesActive(false);
    }

    public void ShowInstruction()
    {
        startMenu.SetActive(false);
        instructionTextView.SetActive(true);
    }
}
