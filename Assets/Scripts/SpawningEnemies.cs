﻿using System.Collections;
using UnityEngine;
using Random = UnityEngine.Random;

public class SpawningEnemies : MonoBehaviour
{
    public GameObject spider;
    public GameObject skeleton;
    public GameObject smoke;
    public GameObject spear;
    private static GameObject spawnEnemy;
    private static GameObject _smoke;
    private static Vector3 spawnpos;
    private Rigidbody rb;
    private float moveSpeed;
    private float gravity;

    void Start()
    {
        rb = GetComponent<Rigidbody>();
        moveSpeed = 1f;
        gravity = 1f;
    }
    
    void FixedUpdate()
    {
        float horizontal = Input.GetAxisRaw("Horizontal");
        float vertical = Input.GetAxisRaw("Vertical");
        Vector3 movement = new Vector3(horizontal, 0.0f, vertical);
        rb.velocity = movement.normalized * moveSpeed * Time.deltaTime;
        
    }
    
    public void SpawnFallingSpears()
    {
        StartCoroutine(SpawnSpear());
    }
    
    public void SpawnASkeleton()
    {
        StartCoroutine(SpawnSkeleton());
    }
    
    public void SpawnSpiders()
    {
        StartCoroutine(SpawnSpider());
    }
    
    IEnumerator SpawnSpider()
    {
        yield return new WaitForSeconds(3f);
        spawnEnemy = Instantiate(spider, SpawnObjectOnPlane.spawnedObject.transform.position, Quaternion.identity);         
        _smoke = Instantiate(smoke, SpawnObjectOnPlane.spawnedObject.transform.position, Quaternion.identity);
        Destroy(_smoke, 1f);
    }
    
    IEnumerator SpawnSkeleton()
    {
        yield return new WaitForSeconds(3f);
        spawnEnemy = Instantiate(skeleton, SpawnObjectOnPlane.spawnedObject.transform.position, Quaternion.identity);         
        _smoke = Instantiate(smoke, SpawnObjectOnPlane.spawnedObject.transform.position, Quaternion.identity);
        Destroy(_smoke, 1f);
    }

    IEnumerator SpawnSpear()
    {
        yield return new WaitForSeconds(5f);
        for (int i = 0; i < 12; i++)
        {
            yield return new WaitForSeconds(1f);
            float x = Random.Range(SpawnObjectOnPlane.spawnedObject.transform.position.x -0.127f, 
                SpawnObjectOnPlane.spawnedObject.transform.position.x + 0.127f);
            float y = 0.3356f;
            float z = Random.Range(SpawnObjectOnPlane.spawnedObject.transform.position.z -0.071f,
                SpawnObjectOnPlane.spawnedObject.transform.position.z + 0.071f);
            Instantiate(spear, new Vector3(x, y, z), Quaternion.Euler(180,0,0));
            StartCoroutine(RemoveSpears());
        }
    }

    IEnumerator RemoveSpears()
    {
        yield return new WaitForSeconds(12f);
        GameObject[] enemies = GameObject.FindGameObjectsWithTag("Spear");
        foreach (GameObject enemy in enemies)
        {
            Destroy(enemy);
        }
    }
}