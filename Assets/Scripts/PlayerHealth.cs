﻿using UnityEngine;

public class PlayerHealth : MonoBehaviour
{
    public int maxHealth = 20;
    public int currentHealth;
    public HealthBar healthBar;
    public GameObject gameLostMenu;
    public GameObject playerView;

    void Start()
    {
        currentHealth = maxHealth;
        healthBar.SetMaxHealth(maxHealth);
    }
    
    public void TakeDamage(int damage)
    {
        currentHealth -= damage;
        healthBar.SetHealth(currentHealth);

        if (currentHealth == 0)
        {
            playerView.SetActive(false);
            Destroy(GameObject.FindGameObjectWithTag("Jumanji"));
            Destroy(GameObject.FindGameObjectWithTag("Skeleton"));
            GameObject[] _spiders = GameObject.FindGameObjectsWithTag("Spider");
            
            foreach (GameObject _spider in _spiders)
            {
                Destroy(_spider);
            }
            
            gameLostMenu.SetActive(true);
        }
    }
}