﻿using UnityEngine;

public class GameWon : MonoBehaviour
{
    public GameObject gameWonMenu;
    
    public void startGameWonMenu()
    {
        gameWonMenu.SetActive(true);
    }
}