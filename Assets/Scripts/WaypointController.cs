﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WaypointController : MonoBehaviour
{
    public List<Transform> playerPathOne = new List<Transform>();
    public List<Transform> playerPathTwo = new List<Transform>();
    public List<Transform> playerPathThree = new List<Transform>();
    public List<Transform> playerPathFour = new List<Transform>();
    public List<Transform> waypoints = new List<Transform>();
    private Transform targetWaypoint;
    private int targetWaypointIndex = 0;
    private float minDistance = 0.125f * 0.019f;
    private int lastWaypointIndex = 0;
    private float movementSpeed = 0.1f / 10f;
    private float rotationSpeed = 1.0f;
    public SphereEventScript jumanjiMessage;
    public GameObject message;
    public GameObject playerView;
    public GameWon gamewon;
    public GameObject eventsystem;

    void Start()
    {
        waypoints = playerPathTwo;
        message = GameObject.Find("Jumanjimessage");
        jumanjiMessage = message.GetComponent<SphereEventScript>();
        eventsystem = GameObject.Find("EventSystem");
        gamewon = eventsystem.GetComponent<GameWon>();
    }
    
    void Update()
    {
        if (targetWaypointIndex <= lastWaypointIndex && targetWaypoint != null)
        {
            if (targetWaypointIndex == waypoints.Count - 2)
            {
                GetComponent<Rigidbody>().useGravity = false;
            }
            float movementStep = movementSpeed * Time.deltaTime;
            float rotationStep = rotationSpeed * Time.deltaTime;
            Vector3 directionToTarget = targetWaypoint.position - transform.position;
            Quaternion rotationToTarget = Quaternion.LookRotation(directionToTarget);
            transform.rotation = Quaternion.Slerp(transform.rotation, rotationToTarget, rotationStep);
            float distance = Vector3.Distance(transform.position, targetWaypoint.position);
            CheckDistanceToWaypoint(distance);
            transform.position = Vector3.MoveTowards(transform.position, targetWaypoint.position, movementStep);
        }
    }

    void CheckDistanceToWaypoint(float currentDistance)
    {
        if (currentDistance <= minDistance && targetWaypointIndex == lastWaypointIndex && targetWaypointIndex < waypoints.Count - 1)
        {
            jumanjiMessage.AnimateMessage();      
        }
        if (currentDistance <= minDistance && targetWaypointIndex < waypoints.Count - 1)
        {
            targetWaypointIndex++;
            targetWaypoint = waypoints[targetWaypointIndex];
        }
        else if (currentDistance <= minDistance && targetWaypointIndex == lastWaypointIndex)
        {
            targetWaypointIndex++;
            jumanjiMessage.Jumanji();
            StartCoroutine(FinishGame());
        }
    }

    public IEnumerator FinishGame()
    {
        yield return new WaitForSeconds(10);
        playerView = GameObject.Find("PlayerView");
        playerView.SetActive(false);
        gamewon.startGameWonMenu();
        Destroy(GameObject.FindGameObjectWithTag("Jumanji"));
        Destroy(GameObject.FindGameObjectWithTag("Skeleton"));
        GameObject[] _spiders = GameObject.FindGameObjectsWithTag("Spider");
            
        foreach (GameObject _spider in _spiders)
        {
            Destroy(_spider);
        }
    }

    public int GetLastWaypointIndex()
    {
        return lastWaypointIndex;
    }

    public void IncreaseLastWayPointIndex(int index)
    {
        lastWaypointIndex += index;
    }

    public void SetTargetWaypoint(Transform waypoint)
    {
        targetWaypoint = waypoint;
    }

    public int GetTargetWaypointIndex()
    {
        return targetWaypointIndex;
    }
}