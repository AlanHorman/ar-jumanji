﻿using System;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.XR.ARSubsystems;

public class ChooseFigureButtons : MonoBehaviour
{
    public Button back;
    public Button lionButton;
    public Button horseButton;

    public GameObject placingBoardMenu;
    public GameObject chooseFigureMenu;
    public GameObject playerView;

    public GameObject planeGenerationMenu;

    public GameObject lion;
    public GameObject horse;
    void Start()
    {
        back.onClick.AddListener(Back);
        lionButton.onClick.AddListener(ChooseLion);
        horseButton.onClick.AddListener(ChooseHorse);
    }
    
    public void Back()
    {
        chooseFigureMenu.SetActive(false);
        planeGenerationMenu.SetActive(true);
        SpawnObjectOnPlane.isPlaced = false;
        SpawnObjectOnPlane.SetAllPlanesActive(true);
        SpawnObjectOnPlane.planeManager.detectionMode = PlaneDetectionMode.Horizontal;

    }

    private void Update()
    {
        SpawnObjectOnPlane.planeManager.detectionMode = PlaneDetectionMode.None;

    }

    public void ChooseLion()
    {
        /*
         * Lion:
         * 
         * Position at PlayerPathOne: (-606.1604, 27.7, 411.1992); Rotation at PlayerPathOne: (0, 98.47601, 0)
         * Position at PlayerPathTwo: (610, 27.7, 414); Rotation at PlayerPathOne: (0, -95.9, 0)
         * Position at PlayerPathThree: (606, 27.7, -420); Rotation at PlayerPathOne: (0, -72.63, 0)
         * Position at PlayerPathFour: (-586, 27.7, -422); Rotation at PlayerPathOne: (0, 72.5, 0)
         */
        
        GameObject parent = GameObject.FindGameObjectWithTag("Jumanji");
        lion = parent.transform.GetChild(6).gameObject;
        lion.transform.position = new Vector3(610, 27.7f, 414);
        lion.transform.Rotate(0, -95.9f, 0);
        lion.SetActive(true);
        Destroy(parent.transform.GetChild(7).gameObject);
        GetComponent<RandomCube>().Setup(lion);

        chooseFigureMenu.SetActive(false);
        playerView.SetActive(true);
    }
    
    public void ChooseHorse()
    {
        GameObject parent = GameObject.FindGameObjectWithTag("Jumanji");
        horse = parent.transform.GetChild(6).gameObject;
        horse.SetActive(true);
        Destroy(parent.transform.GetChild(7).gameObject);
        GetComponent<RandomCube>().Setup(horse);
        
        chooseFigureMenu.SetActive(false);
        playerView.SetActive(true);
    }
}