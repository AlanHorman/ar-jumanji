# Jumanji AR

## Table of Contents  
- [Introduction](#introduction) 
- [Goal](#goal)
- [Control](#control)
- [Features](#features)
- [Updates](#updates)
- [Future](#future)
- [Notes](#notes)

## Introduction
[![](http://i3.ytimg.com/vi/SSSkzh4nEUM/hqdefault.jpg)](https://www.youtube.com/watch?v=SSSkzh4nEUM&t=16s)

This is an AR Game based on the movie of the same name from the year
1995 which in turn is based on the novel by Chris Van Allsburg. 

The game was created with **Unity 2019.3.4f1** and **ARFoundation** and was
developed by Ram�n Wilhelm and Oliver Kovarna.

If you want to play this game you need an Android Device with 
at least 7.0 Nougat or higher, which also supports ARCore.

Jumanji (1995) is a registered trademark of TriStar Production Inc.
The AR Game is a master student project from the [Fulda University of Applied Sciences](https://www.hs-fulda.de/en/home)
and is not allowed for commercial use.

## Goal
In this game, you first scan the room to project ARPlanes. On this tarp you tap on a specific spot to place the game board. 
If necessary, you can also rotate or move the game board.
Next, one chooses one of two game figures, which are dragged and dropped to the starting point. After that two weapons
- a sword
- a shield

and the dice are spawned. All three objects can be rotated and moved via drag and drop. 
The dice can be dropped on the board or the ARPlane. As in the original 1995 movie, the game figure moves forward. 
Three messages appear randomly in the green crystal ball. Either spears, a skeleton or a spider are spawned to the respective messages. 
The goal is to get the character to the crystal ball and to defend it. The character can be defended with the shield and the sword. 
To attack with the sword, the sword must be moved and moved down with a second touch.

## Features
* ARPlanes
* Two game figures, a horse and a lion
* Sword and shield
* Dice
* Health bar
* Two enemies, skeleton and spider
* Falling spears
* Messages in crystal ball

## Updates
No Updates yet.

## Future
An alternative to Jumanji is being developed, closer to the original movie, where the player defends himself instead of the character. 
More traps and dangers will be spawned. Custom animations and models will be created for this game.

## Notes
* Make sure, you open the game with [**Unity 2019.3.4f1 (64-bit)**](https://unity3d.com/de/get-unity/download/archive).
* Most of the Models are from the [**Unity Assetstore**](https://assetstore.unity.com/) and [**Sketchfab**](https://sketchfab.com).
* Jumanji is developed with [**ARFoundation**](https://docs.unity3d.com/Packages/com.unity.xr.arfoundation@4.1/manual/index.html#about-ar-foundation).
* [**Lean Touch**](https://assetstore.unity.com/packages/tools/input-management/lean-touch-30111) is implemented for the control of the game. 
* This project has been stored via [**Git LFS**](https://git-lfs.github.com/).
